class Board
  attr_accessor :grid

  def [](pos)
    @grid[pos[0]][pos[1]]
  end


  def initialize(input_grid = nil)
    if input_grid == nil
      #go with a default grid if nil
      @grid = Board.default_grid
    else
      @grid = input_grid
    end
  end


  def self.default_grid
    outer = Array.new(10) {Array.new(10, nil)}
    outer
  end

  def count
    ctr = 0
    (0..@grid.length - 1).each do |idx|
      (0..@grid[idx].length - 1).each do |idx2|
        ctr += 1 if @grid[idx][idx2] == :s
      end
    end
    ctr
  end

  def full?
    flattened = @grid.flatten
    if flattened.any?{|x| x == nil}
      return false
    end
    return true
  end


  def place_random_ship
    if full?
      puts @grid.to_s
      raise "full board, cannot place additional ship"
      return nil
    end
    coord = gen_random_coord
    @grid[coord[0]][coord[1]] = :s
  end


  def gen_random_coord
    len = @grid.length
    [rand(len), rand(len)]
  end

  def won?
    if count == 0
      return true
    end
    return false
  end

  def empty?(i = nil)
    if i.class == Array
      if grid[i[0]][i[1]] != :s
        return true
      end
      return false
    end

    if @grid.flatten.all?{|i| i == nil}
      return true
    end
    return false
  end
end
