class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    puts "attack enemy position, i.e. '2,3'"
    coord = @player.get_play
    attack(coord)
  end

  def get_play(guess)
    coord = guess.split(",")
    coord.map!{|i| i.to_i}
  end

  def attack(coord)
    @board.grid[coord[0]][coord[0]] = :x
    return coord
  end

end
